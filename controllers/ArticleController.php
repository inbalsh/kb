<?php

namespace app\controllers;

use Yii;
use app\models\Article;
use app\models\ArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\filters\AccessControl;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * @inheritdoc
     */
public function behaviors()
    {
        return [ // מערך שהאיבר הראשון שלו הוא ורבס
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [ // כאן מגדירים למי מותר להיכנס לאן
                'class' => AccessControl::className(),
                'only' => ['update'], // חל רק על עדכון. ניתן להוסיף עוד בתוך הסוגריים עם הפרדת פסיקים
                'rules' => [
                    [
                        'allow' => true, //תאפשר
                        'actions' => ['update'], // לעשות עדכון
                        'roles' => ['updateArticle'], // למי שיש את ההרשאה אפדייטארטיקל
                        'roleParams' => function() {     //אם אין חוק, אין צורך בשתי השורות הבאות
                            return ['article' => Article::findOne(['id' => Yii::$app->request->get('id')])]; // רלוונטית רק להרשאה עם חוק
                                                                     // מעבירים פרמטר באיזה ארטיקל מדובר
                        },
                    ],
              /*                         [
                        'allow' => true, //תאפשר
                        'actions' => ['create'],
                        'roles' => ['createArticle'], 
                                            ],
      */                                  
    ],
            ],
        ];            
    }


    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams); //כל הפרמטרים מה url
                        // הפעלת הפונקציה סרצ' של הסרצ' מודל, אליה נכניס את כל הפרמטרים שהגיעו מה url
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id); // למודל נכנס המאמר לפי ה id
        $tagModels = $model->tags;      // דאגנו מראש לייצר גטר במודל של ארטיקל, בזכותו נגיע לתגיות של מאמר מסוים ע"י שורה זו.
                                        // מאחורה עומדת שאילתא עם JOIN
        $tags = '';     //יתחיל כריק ואז נמלא אותו
        foreach($tagModels as $tag){ // נרצה להפוך את התג הזה ללינק
            $tagLink = Html::a($tag->name,['article/index' , 'ArticleSearch[tag]'=> $tag->name ]);
  
            // הפונקציה יודעת לבנות לנו לינק - קישור של html
                                            // אנו צריכים שם ו url
                                           //כאשר נלחץ על הקישור ניימ נגיע לקונטרולר של ארטיקל ונפעיל עליו את האקשן של אינדקס
/*קישור בנוי משני פרמטרים: הטקסט על הקישור שזה התווית - השם ובנוסף לאן הקישור מוביל שזה הכתובת        
יידע ללכת לאקשן אינדקס, יוסיף לה את הפרמטר, שם זה יזוהה בשם טאג
הקי זה ארטיקלסירצטאג והערך (ואליו) הינו טאג נייצ
*/
     
            $tags .= ','.$tagLink;  // יצרנו כאן מערך של שרשורים

        }

        $tags = substr($tags,1); // מוחק את התו הראשון מהמערך מכיוון שהוא פסיק

        return $this->render('view', [
            'model' => $this->findModel($id),
            'tags' => $tags
            // שלב הבא ללכת לויו ולממש את המערך
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();

  //      if (\Yii::$app->user->can('createArticles',['article' => $model])){ // צריך לשים את זה לאחר השורה של המודל כי נשתמש במודל הזה
    
                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('create', [
                'model' => $model,
            ]);
    //    }
   //     throw new ForbiddenHttpException('Sorry, you are not allowed to update article. this is not your article');
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) 
    {
        $model = $this->findModel($id); // מציאת המאמר הרלוונטי

    //   if (\Yii::$app->user->can('updateArticles',['article' => $model])){ // צריך לשים את זה לאחר השורה של המודל כי נשתמש במודל הזה
    // מעבירים את כל המאמר אל החוק

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
   //     }
     //   throw new ForbiddenHttpException('Sorry, you are not allowed to update article. this is not your article');

    }
    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
