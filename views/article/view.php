<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Article */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

<!-- הויו מכיר רק את המשתנים שהקונטרולר שלח לו-->
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'descriptin',
            'body:ntext',
            'author_id',
            'editor_id',    
            [
                'label' => 'Category',
                'value' => $model->category->name
            ],
            [
                'label' => 'Tags',
                'format' => 'html',
                'value' => $tags,
            ],

            //'created_at',
            //'updated_at',
            [
               'attribute' => 'created_at', 
               'value' => date('Y-m-d H:i:s', $model->created_at)
            ],
            [
               'attribute' => 'updated_at',
               'value' => date('Y-m-d H:i:s', $model->updated_at)
            ], 
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
