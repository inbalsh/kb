<?php

use yii\db\Migration;

/**
 * Class m180429_163012_change_timestemp_to_integer
 */
class m180429_163012_change_timestemp_to_integer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {//שינוי סוג השדה
        $this->alterColumn('article','created_at',$this->integer());
        $this->alterColumn('article','updated_at',$this->integer());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180429_163012_change_timestemp_to_integer cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180429_163012_change_timestemp_to_integer cannot be reverted.\n";

        return false;
    }
    */
}
