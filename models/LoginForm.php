<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username; //נגדיר שדות כי אנחנו לא מחוברים לטבלת מסד הנתונים
    public $password;
    public $rememberMe = true;

    private $_user = false;

// לאוד מחזיקה את שלושת המשתנים הנל
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
           // חייב לעמוד בדרישות של ולידייט פסוורד, אנחנו יצרנו את הפונקציה הזו
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params) //החוקים של ולידייט פסוורד
    {
        if (!$this->hasErrors()) {  //אם לא היו שגיאות עד עכשיו
            $user = $this->getUser();    //משוך את היוזר באמצעות גאט יוזר

        // מפעילים את השיטה ולידייט פסוורד שבמחלקה ויזר. !יוזר- יקבל פולס אם אין יוזר כזה. || במחלקה יוזר נוודא את הסיסמה
            if (!$user || !$user->validatePassword($this->password)) { // שונה מהולידטפסוורד שיש 2 שורות למעלה
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()  // השיטה שאנחנו מפעילים מסייט קונטרולר
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
